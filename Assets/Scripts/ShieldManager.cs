﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldManager : MonoBehaviour
{
    [SerializeField] GameObject shield;
    [SerializeField] GameObject clock;
    public float burbuja;
    public float currentTime = 0;
    public float time;
    public float escudo;
    public float reloj;

    void Update()
    {
        escudo = PlayerPrefs.GetFloat("shield", 0);
        reloj = PlayerPrefs.GetFloat("clock", 0);

        if(escudo == 1 && reloj == 1){
            currentTime += Time.deltaTime;
        }
        else if(escudo == 1 && reloj == 0)
        {
            currentTime += Time.deltaTime;
        }
        else if(escudo == 0 && reloj == 1)
        {
            currentTime += Time.deltaTime;
        }
        else if(escudo == 0 && reloj == 0)
        {
            currentTime = 0f;
        }
        
        if (currentTime >= time)
        {
            currentTime = 0;

            if(burbuja == 1)
            {
                burbuja++;
                if (escudo == 1)
                {
                    Instantiate(shield, new Vector3(12, Random.Range(-5.2f, 3.6f), -1), Quaternion.identity, this.transform);
                }
                else if(escudo == 0)
                {
                    if(reloj == 1)
                    {
                        Instantiate(clock, new Vector3(12, Random.Range(-5.2f, 3.6f), -1), Quaternion.identity, this.transform);
                    }
                }
            }
            else if (burbuja == 2)
            {
                burbuja = 1;
                if(reloj == 1)
                {
                    Instantiate(clock, new Vector3(12, Random.Range(-5.2f, 3.6f), -1), Quaternion.identity, this.transform);
                }
                else if (reloj == 0)
                {
                    if(escudo == 1)
                    {
                        Instantiate(clock, new Vector3(12, Random.Range(-5.2f, 3.6f), -1), Quaternion.identity, this.transform);
                    }
                }
            }
        }   
    }
}
