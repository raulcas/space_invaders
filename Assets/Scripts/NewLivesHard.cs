﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NewLivesHard : MonoBehaviour
{
    private float lives;
    [SerializeField] GameObject live;
    public float currentTime = 0;
    public float contador;
    private float record;

    private void Awake()
    {
        record = PlayerPrefs.GetFloat("record", 0);
    }

    void Update()
    {
        lives = PlayerPrefs.GetFloat("lives", 0);

        if (lives < 2){
            currentTime += Time.deltaTime;
            if (currentTime >= contador){
                Instantiate(live, new Vector3(12, Random.Range(-5.2f, 3.6f), -1), Quaternion.identity, this.transform);
                currentTime = 0;
            }
        }
        if(lives == 0)
        {
            PlayerPrefs.SetFloat("record", record);
            StartCoroutine(FinishGame());
        }
    }

    IEnumerator FinishGame()
    {
        yield return new WaitForSeconds(1.0f);
        SceneManager.LoadScene("GameOver");
    }
}
