﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Musica : MonoBehaviour
{
    [SerializeField] AudioSource aS;
    public int music;
    private int parar = 0;

    void Awake()
    {
        music = PlayerPrefs.GetInt("music", 0);

        if (music == 1)
        {
            aS.Play();
        }

        parar = 0;
    }

    private void Update()
    {
        PlayerPrefs.SetInt("musica", music);
        if (music == 0)
        {
            aS.Stop();
        }
        if(parar == 0)
        {
            parar++;
            if (music == 1)
            {
                aS.Play();
            }
        }
    }
}
