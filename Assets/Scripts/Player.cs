﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    private Vector2 axis;
    public float speed;
    public Vector2 limits;
    public float limits2;

    public Propeder prop;

    private int navecolor;
    private int music = 1;
    private int effects = 1;

    private float shootTime=0;
    public Weapon weapon;
    public Weapon weapon2;
    public Weapon weapon3;
    public Weapon weapon4;

    private bool iamDead = false;

    [SerializeField] Text rocket;
    [SerializeField] Text triple;
    [SerializeField] Text super;
    [SerializeField] GameObject shield;
    [SerializeField] GameObject clock;

    [SerializeField] GameObject[] graphics;
    [SerializeField] Collider2D collider;
    [SerializeField] ParticleSystem ps;
    [SerializeField] ParticleSystem psHeal;
    [SerializeField] ParticleSystem psShield;
    [SerializeField] ParticleSystem psClock;
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioSource audioSource2;
    [SerializeField] AudioSource audioSource3;
    [SerializeField] AudioSource audioSource4;

    private ScoreManager sm;
    [SerializeField] CanvasGroup cg;
    public float fadeSpeed;
    private float actualAlfa = 1f;

    public int lives = 3;
    private bool shielded = false;
    private float escudo = 1;
    private bool savedClock = false;
    private float reloj = 1;

    float tripleBullet = 0;
    float rocketBullet = 0;
    float superBullet = 0;

    private void Awake()
    {
        cg.alpha = 0f;
        shield.SetActive(false);
        clock.SetActive(false);

        navecolor = PlayerPrefs.GetInt("navecolor", 0);
        music = PlayerPrefs.GetInt("music", 0);
        effects = PlayerPrefs.GetInt("effects", 0);

        for (int kk = 0; kk < graphics.Length; kk++)
        {
            graphics[kk].SetActive(false);
        }

        graphics[navecolor].SetActive(true);

        sm = (GameObject.Find("ScoreManager")).GetComponent<ScoreManager>();
    }

    private void Update()
    {
        PlayerPrefs.SetFloat("lives", lives);
        PlayerPrefs.SetFloat("shield", escudo);
        PlayerPrefs.SetFloat("clock", reloj);

        PlayerPrefs.SetInt("nave", navecolor);
        PlayerPrefs.SetInt("musica", music);
        PlayerPrefs.SetInt("efectos", effects);

        if (iamDead)
        {
            return;
        }

        if(savedClock == true)
        {
            if (Input.GetKeyDown(KeyCode.P))
            {
                clock.SetActive(false);
                if (effects == 1)
                {
                    audioSource2.Play();
                }
                reloj = 1f;
                speed += 20;
                StartCoroutine(timeReduction());
                speed -= 20;
            }
        }

        cg.alpha -= fadeSpeed * Time.deltaTime;
        if(cg.alpha == 0)
        {
            cg.alpha = 0f;
        }

        shootTime += Time.deltaTime;
        
        transform.Translate(axis * speed * Time.deltaTime);

        if (transform.position.x > limits.x)
        {
            transform.position = new Vector3(limits.x, transform.position.y, transform.position.z);
            if(!shielded){
                lives--;
                StartCoroutine(DestroyShip());
            }
            if(shielded){
                shield.SetActive(false);
                if (effects == 1)
                {
                    audioSource2.Play();
                }
                shielded = false;
                escudo = 1f;
            }
        }
        else if (transform.position.x < -limits.x)
        {
            transform.position = new Vector3(-limits.x, transform.position.y, transform.position.z);
            if(!shielded){
                lives--;
                StartCoroutine(DestroyShip());
            }
            if(shielded){
                shield.SetActive(false);
                if (effects == 1)
                {
                    audioSource2.Play();
                }
                shielded = false;
                escudo = 1f;
            }
        }

        if (transform.position.y < -limits.y)
        {
            transform.position = new Vector3(transform.position.x, -limits.y, transform.position.z);
        }
        else if (transform.position.y > limits2)
        {
            transform.position = new Vector3(transform.position.x, limits2, transform.position.z);
        }

        if (axis.x > 0)
        {
            prop.BlueFire();
        }
        else if (axis.x < 0)
        {
            prop.RedFire();
        }
        else
        {
            prop.Stop();
        }

        rocket.text = rocketBullet.ToString("0");
        triple.text = tripleBullet.ToString("0");
        super.text = superBullet.ToString("0");
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemy")
        {
            if(!shielded){
                lives--;
                StartCoroutine(DestroyShip());
            }
            if(shielded){
                shield.SetActive(false);
                if (effects == 1)
                {
                    audioSource2.Play();
                }
                shielded = false;
                escudo = 1f;
            }
        }
        else if (other.tag == "EnemyBullet")
        {
            if(!shielded){
                lives--;
                StartCoroutine(DestroyShip());
            }
            if(shielded){
                shield.SetActive(false);
                if (effects == 1)
                {
                    audioSource2.Play();
                }
                shielded = false;
                escudo = 1f;
            }
        }
        else if (other.tag == "2attack")
        {
            rocketBullet += 2;
            if (effects == 1)
            {
                audioSource4.Play();
            }
        }
        else if (other.tag == "3attack")
        {
            tripleBullet += 5;
            if (effects == 1)
            {
                audioSource4.Play();
            }
        }
        else if (other.tag == "4attack")
        {
            superBullet += 3;
            if (effects == 1)
            {
                audioSource4.Play();
            }
        }
        else if (other.tag == "Shield")
        {
            shielded = true;
            shield.SetActive(true);
            if (effects == 1)
            {
                audioSource3.Play();
            }
            psShield.Play();
            escudo = 0f;

        }
        else if (other.tag == "Clock")
        {
            savedClock = true;
            clock.SetActive(true);
            if (effects == 1)
            {
                audioSource3.Play();
            }
            psClock.Play();
            reloj = 0f;
        }
        else if (other.tag == "Live")
        {
            lives++;
            if (effects == 1)
            {
                audioSource4.Play();
            }
            psHeal.Play();
        }
    }

    IEnumerator DestroyShip()
    {
        iamDead = true;

        if (lives > 0)
        {
            cg.alpha = 1f;
        }

        graphics[navecolor].SetActive(false);

        GetComponent<Collider2D>().enabled = false;

        prop.gameObject.SetActive(false);

        ps.Play();

        if(effects == 1)
        {
            audioSource.Play();
        }

        yield return new WaitForSeconds(1.5f);

        if (lives > 0)
        {
            StartCoroutine(inMortal());

            cg.alpha = actualAlfa;
        }
    }

    IEnumerator inMortal()
    {
        //Vuelvo a activar el jugador
        iamDead = false;

        graphics[navecolor].SetActive(true);
        //Activo el propeller
        prop.gameObject.SetActive(true);

        for (int i = 0; i < 15; i++)
        {
            graphics[navecolor].SetActive(false);
            yield return new WaitForSeconds(0.1f);
            graphics[navecolor].SetActive(true);
            yield return new WaitForSeconds(0.1f);
        }
        collider.enabled = true;
    }

    IEnumerator timeReduction()
    {
        Time.timeScale = 0.5f;
        yield return new WaitForSecondsRealtime(5f);
        Time.timeScale = 1.0f;
        savedClock = false;
    }

    public void Shoot()
        {
            if (shootTime > weapon.GetCadencia())
            {
                shootTime = 0f;
                weapon.Shoot();
            }
        }
    public void Disparo()
    {
        if (tripleBullet > 0)
        {
            if (shootTime > weapon2.GetCadencia())
            {
                shootTime = 0f;
                weapon2.Shoot();
                tripleBullet--;
            }
        }
    }

    public void Tiro()
    {
        if(superBullet > 0)
        {
            if (shootTime > weapon3.GetCadencia())
            {
                shootTime = 0f;
                weapon3.Shoot();
                superBullet--;
            }
        }
    }

    public void Balaso()
    {
        if(rocketBullet > 0)
        {
            if (shootTime > weapon4.GetCadencia())
            {
                shootTime = 0f;
                weapon4.Shoot();
            }
        }
    }

    public void SetAxis(Vector2 input)
    {
        axis = input;
    }
}
