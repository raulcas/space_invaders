﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager1 : MonoBehaviour
{
    [SerializeField] GameObject enemy;
    [SerializeField] GameObject enemy2;
    [SerializeField] GameObject[] boss;
    public float timeLaunchEnemy;
    private float currentTime = 0;
    private float formation = 0;
    private int salidaBoss = 0;
    private int elegido;

    void Update()
    {
        elegido = Random.Range(0, boss.Length);

        for (int kk = 0; kk < boss.Length; kk++)
        {
            boss[kk].SetActive(false);
        }

        boss[elegido].SetActive(true);

        currentTime += Time.deltaTime;
        
        if(salidaBoss == 1)
        {
            timeLaunchEnemy = 2f;
        }
        if (salidaBoss == 2)
        {
            timeLaunchEnemy = 1.5f;
        }
        if (salidaBoss == 3)
        {
            timeLaunchEnemy = 1f;
        }
        if (salidaBoss == 4)
        {
            timeLaunchEnemy = 0.5f;
        }

        if (currentTime >= timeLaunchEnemy)
        {
            if (formation < 10)
            {
                currentTime = 0;
                formation++;
                Instantiate(enemy, new Vector3(12, Random.Range(-5.2f, 3.6f), -1), Quaternion.identity, this.transform);
            }
            else if (formation > 10 && formation < 20)
            {
                currentTime = 0;
                formation++;
                Instantiate(enemy, new Vector3(12, Random.Range(-5.2f, 3.6f), -1), Quaternion.identity, this.transform);
            }
            else if (formation == 10)
            {
                currentTime = 0;
                formation++;
                Instantiate(enemy2, new Vector3(12, Random.Range(-5.2f, 3.6f), -1), Quaternion.identity, this.transform);
            }
            else if (formation == 20)
            {
                formation++;
                currentTime = 0;
                Instantiate(boss[elegido], new Vector3(12, 0, -1), Quaternion.identity, this.transform);
                salidaBoss++;
            }
            else if(formation == 21)
            {
                formation = 0;
                currentTime = 0;
                timeLaunchEnemy -= 0.04f;
            }
        }
    }
}
