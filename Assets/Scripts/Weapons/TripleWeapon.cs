﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TripleWeapon : Weapon
{

    public GameObject laserBullet;
    public float cadencia;
    public AudioSource audioSource;
    private int effects = 1;

    private void Awake()
    {
        effects = PlayerPrefs.GetInt("effects", 0);
    }

    public override float GetCadencia()
    {
        return cadencia;
    }

    public override void Shoot()
    {
        Instantiate(laserBullet, this.transform.position, Quaternion.identity, null);
        GameObject go = Instantiate(laserBullet, this.transform.position, Quaternion.identity, null);
        go.transform.Rotate(0, 0, 15);
        GameObject go2 = Instantiate(laserBullet, this.transform.position, Quaternion.identity, null);
        go2.transform.Rotate(0, 0, -15);

        if (effects == 1)
        {
            audioSource.Play();
        }
    }
}