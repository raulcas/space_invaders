﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background1 : MonoBehaviour
{
    [SerializeField] Material mt;
    public float scrollSpeed;
    float offset;

    private float axisX;

    private void Start()
    {
        offset = PlayerPrefs.GetFloat("offset", 0);
    }

    private void Update()
    {
        offset += Time.deltaTime * scrollSpeed;
        mt.SetTextureOffset("_MainTex", new Vector2(offset, 0));

        PlayerPrefs.SetFloat("offsetover", offset);
    }

    public void SetVelocity(float velx)
    {
        axisX = velx;
    }
}