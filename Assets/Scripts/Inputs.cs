﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inputs : MonoBehaviour
{
    private Vector2 axis;

    [SerializeField] Player player;
    [SerializeField] Background1 background;

    Vector2 input;


    void Update()
    {
        input.x = Input.GetAxis("Horizontal");
        input.y = Input.GetAxis("Vertical");

        if(Input.GetButton("Fire1"))
        {
            player.Shoot();
        }
        if (Input.GetButton("Fire2"))
        {
            player.Disparo();
        }

        if (Input.GetButton("Fire3"))
        {
            player.Tiro();
        }

        if (Input.GetButton("Jump"))
        {
            player.Balaso();
        }

        player.SetAxis(input);
        background.SetVelocity(axis.x);
    }
}
