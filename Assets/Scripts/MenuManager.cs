﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    [SerializeField] GameObject canvas;
    [SerializeField] GameObject canvas2;

    public void PrimerStart()
    {
        canvas2.SetActive(true);
        Time.timeScale = 0.0f;
    }
    public void PrimerStartAtras()
    {
        canvas2.SetActive(false);
        Time.timeScale = 1.0f;
    }

    public void PulsaStart()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("GameEasy");
    }

    public void PulsaStartHard()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("GameHard");
    }

    public void PulsaExit(){
        Application.Quit();
    }

    public void PulsaOpciones()
    {
        SceneManager.LoadScene("Options");
    }

    public void PulsaOpcionesEasy()
    {
        SceneManager.LoadScene("OptionsEasy");
    }

    public void PulsaOpcionesHard()
    {
        SceneManager.LoadScene("OptionsHard");
    }

    public void TitleReturn()
    {
        SceneManager.LoadScene("TitleScreen");
    }

    public void Credits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void PrimerExit()
    {
        canvas.SetActive(true);
        Time.timeScale = 0.0f;
    }

    public void PulsaVolver()
    {
        canvas.SetActive(false);
        Time.timeScale = 1.0f;
    }
}
