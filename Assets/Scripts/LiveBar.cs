﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiveBar : MonoBehaviour
{
    [SerializeField] GameObject[] barraVida;
    private float lives;

    void Update()
    {
        lives = PlayerPrefs.GetFloat("lives", 0);

        for (int kk = 0; kk < barraVida.Length; kk++)
        {
            barraVida[kk].SetActive(false);
        }

        if (lives == 3)
        {
            barraVida[1].SetActive(true);
        }
        else if(lives == 2)
        {
            barraVida[2].SetActive(true);
        }
        else if (lives == 1)
        {
            barraVida[3].SetActive(true);
        }
        else if (lives == 0)
        {
            barraVida[0].SetActive(true);
        }
    }
}
