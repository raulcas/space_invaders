﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IniciarFondo : MonoBehaviour
{
    [SerializeField] Material mt;
    private float offset = 0;

    private void Awake()
    {
        mt.SetTextureOffset("_MainTex", new Vector2(offset, 0));
    }
}
