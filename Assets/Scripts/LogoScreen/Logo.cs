﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Logo : MonoBehaviour
{
    [SerializeField] CanvasGroup cg;
    public float fadeSpeed;
    public float mainTime = 0;
    private int music = 1;
    private int effects = 1;
    private int nave = 0;

    private void Awake()
    {
        cg.alpha = 0f;
        music = 1;
        effects = 1;
        nave = 0;
    }

    void Update()
    {
        PlayerPrefs.SetInt("navecolor", nave);
        PlayerPrefs.SetInt("music", music);
        PlayerPrefs.SetInt("effects", effects);

        mainTime += Time.deltaTime;

        if(mainTime < 4)
        {
            cg.alpha += fadeSpeed * Time.deltaTime;
        }
        if (mainTime >= 4)
        {
            cg.alpha -= fadeSpeed * Time.deltaTime;
        }
        if (mainTime >= 6)
        {
            SceneManager.LoadScene("TitleScreen");
        }

        if (Input.anyKeyDown)
        {
            SceneManager.LoadScene("TitleScreen");
        }
    }
}
