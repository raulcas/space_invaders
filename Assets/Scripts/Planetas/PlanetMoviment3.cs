﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetMoviment3 : MonoBehaviour
{
    public float speed;

    void Update()
    {
        transform.Translate(-speed * Time.deltaTime, 0, 0);

        if(transform.position.x <= -25)
        {
            transform.position = new Vector3(12.72F, 1.57F, 0);
        }
    }
}
