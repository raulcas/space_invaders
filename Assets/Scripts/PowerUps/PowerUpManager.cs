﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpManager : MonoBehaviour
{
    [SerializeField] GameObject bullet;
    public float timeLaunchEnemy;
    private float currentTime = 0;

    void Update()
    {
        currentTime += Time.deltaTime;
        if (currentTime >= timeLaunchEnemy)
        {
            currentTime = 0;
            Instantiate(bullet, new Vector3(12, Random.Range(-5.2f, 3.8f), -1), Quaternion.identity, this.transform);
        }
    }
}
