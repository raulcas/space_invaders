﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp1 : MonoBehaviour
{
    public float speed;

    [SerializeField] CircleCollider2D collider;
    [SerializeField] GameObject sprite;
    private int effects = 1;

    void Update()
    {
        transform.Translate(-speed * Time.deltaTime, 0, 0);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            StartCoroutine(DestroyShip());
        }
        else if (other.tag == "Wall")
        {
            Destroy(this.gameObject);
        }
    }

    IEnumerator DestroyShip()
    {
        //Desactivo el grafico
        sprite.SetActive(false);

        //Elimino el BoxCollider2D
        collider.enabled = false;

        //Lanzo sonido de explosion

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.5f);

        Destroy(this.gameObject);
    }
}
