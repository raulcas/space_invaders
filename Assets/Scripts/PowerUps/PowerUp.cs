﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    public float speed;

    [SerializeField] CircleCollider2D collider;
    [SerializeField] GameObject sprite;
    private ScoreManager sm;
    private int effects = 1;

    private void Awake()
    {
        sm = (GameObject.Find("ScoreManager")).GetComponent<ScoreManager>();
    }

    void Update()
    {
        transform.Translate(-speed * Time.deltaTime, 0, 0);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            StartCoroutine(DestroyShip());
        }
        else if (other.tag == "Wall")
        {
            Destroy(this.gameObject);
        }
    }

    IEnumerator DestroyShip()
    {
        //Desactivo el grafico
        sprite.SetActive(false);
        sm.AddScore(10);
        //Elimino el BoxCollider2D
        collider.enabled = false;

        //Lanzo sonido de explosion

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.5f);

        Destroy(this.gameObject);
    }
}
