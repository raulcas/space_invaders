﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FondoOver : MonoBehaviour
{
    [SerializeField] Material mt;
    private float offset = 0;

    private void Awake()
    {
        offset = PlayerPrefs.GetFloat("offsetover", 0);
    }
    private void Update()
    {
        mt.SetTextureOffset("_MainTex", new Vector2(offset, 0));
    }
}
