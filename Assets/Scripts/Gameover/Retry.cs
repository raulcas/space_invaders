﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Retry : MonoBehaviour
{
    private float score;
    private float recscore;
    [SerializeField] Text puntos;
    [SerializeField] Text record;

    private void Start()
    {
        score = PlayerPrefs.GetFloat("score", 0);
        recscore = PlayerPrefs.GetFloat("record", 0);
    }
    public void RetryGame(){
       SceneManager.LoadScene("GameEasy");
    }

    public void RetryGameHard()
    {
        SceneManager.LoadScene("GameHard");
    }

    public void ExitToMenu(){
        SceneManager.LoadScene("TitleScreen");
    }

    private void Update()
    {
        if(score >= recscore)
        {
            recscore = score;
        }
        PlayerPrefs.SetFloat("record", recscore);

        puntos.text = score.ToString("000000");
        record.text = recscore.ToString("000000");
    }
}
