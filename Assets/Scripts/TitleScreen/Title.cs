﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Title : MonoBehaviour
{
    public float speed;
    [SerializeField] GameObject letter;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(speed * Time.deltaTime, 0, 0);

        if (transform.position.x > 1425)
        {
            transform.position = new Vector3(490, 757.8f, 0);
        }
    }
}