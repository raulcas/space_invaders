﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Backgroundop : MonoBehaviour
{
    [SerializeField] Material mt;
    public float scrollSpeed;
    float offset = 0;

    private float axisX;

    private void Awake()
    {
        offset = PlayerPrefs.GetFloat("offset", 0);
    }

    private void Update()
    {
        offset += Time.deltaTime * scrollSpeed;
        mt.SetTextureOffset("_MainTex", new Vector2(offset, 0));

        PlayerPrefs.SetFloat("offsetop", offset);
    }

    public void SetVelocity(float velx)
    {
        axisX = velx;
    }
}