﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Options : MonoBehaviour
{
    private int nave;
    private int music = 1;
    private int effects = 1;

    private void Awake()
    {
        nave = PlayerPrefs.GetInt("nave", 0);
        music = PlayerPrefs.GetInt("musica", 0);
        effects = PlayerPrefs.GetInt("efectos", 0);
    }

    private void Update()
    {
        PlayerPrefs.SetInt("navecolor", nave);
        PlayerPrefs.SetInt("music", music);
        PlayerPrefs.SetInt("effects", effects);
    }

    public void MusicOn()
    {
        music = 1;
    }
    public void MusicOff()
    {
        music = 0;
    }

    public void EffectsOn()
    {
        effects = 1;
    }
    public void EffectsOff()
    {
        effects = 0;
    }

    public void Verde()
    {
        nave = 0;
    }
    public void Naranja()
    {
        nave = 1;
    }
    public void Azulfuerte()
    {
        nave = 2;
    }
    public void Azulflojo()
    {
        nave = 3;
    }
    public void Rosa()
    {
        nave = 4;
    }
    public void Amarillo()
    {
        nave = 5;
    }
}
