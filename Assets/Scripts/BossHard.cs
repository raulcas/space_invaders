﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossHard : MonoBehaviour
{
    [SerializeField] GameObject[] sprite;
    [SerializeField] GameObject vida1;
    [SerializeField] GameObject vida2;
    [SerializeField] GameObject vida3;
    private int elegido;
    private bool Imdead = false;
    private int disparo = 2;

    [SerializeField] BoxCollider2D collider;
    [SerializeField] ParticleSystem ps;
    [SerializeField] ParticleSystem ps2;
    [SerializeField] ParticleSystem ps3;
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioSource audioSource2;

    private ScoreManager sm;
    private float timeCounter;
    private float timeToShoot;

    private float timeShooting;
    private float speedx;

    private bool isShooting;

    [SerializeField] GameObject bullet;
    private int effects = 1;

    private void Awake()
    {
        effects = PlayerPrefs.GetInt("effects", 0);

        elegido = Random.Range(0, sprite.Length);

        for (int kk = 0; kk < sprite.Length; kk++)
        {
            sprite[kk].SetActive(false);
        }

        sprite[elegido].SetActive(true);
        

        sm = (GameObject.Find("ScoreManager")).GetComponent<ScoreManager>();

        Inicitialization();
    }

    protected virtual void Inicitialization()
    {
        timeCounter = 0.0f;
        timeToShoot = 0.5f;
        timeShooting = 0.5f;
        speedx = 4.0f;
        isShooting = false;
    }

    protected virtual void EnemyBehaviour()
    {
        timeCounter += Time.deltaTime;

        if (Imdead == true)
        {
            return;
        }

        if (timeCounter > timeToShoot)
        {
            if (!isShooting)
            {
                isShooting = true;
                if (Imdead == false)
                {
                    GameObject go = Instantiate(bullet, this.transform.position, Quaternion.identity, null);
                    go.transform.Translate(-1, 0.6f, 0);
                    if (effects == 1)
                    {
                        audioSource2.Play();
                    }
                }
            }
            if (timeCounter > (timeToShoot + timeShooting))
            {
                timeCounter = 0.0f;
                isShooting = false;
            }
        }
        else
        {
            transform.Translate(-speedx * Time.deltaTime, 0, 0);
        }

    }

    void Update()
    {
        EnemyBehaviour();
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bullet")
        {
            if(disparo == 2)
            {
                disparo = 1;
                vida1.SetActive(false);
                ps2.Play();
            }
            else if(disparo == 1)
            {
                disparo = 0;
                vida2.SetActive(false);
                ps3.Play();
            }
            else if(disparo == 0)
            {
                StartCoroutine(DestroyShip());
            }
        }
        else if (other.tag == "Bullet2")
        {
            if (disparo == 2)
            {
                disparo = 1;
                vida1.SetActive(false);
                ps2.Play();
            }
            else if (disparo == 1)
            {
                disparo = 0;
                vida2.SetActive(false);
                ps3.Play();
            }
            else if (disparo == 0)
            {
                StartCoroutine(DestroyShip2());
            }
        }
        else if (other.tag == "Player")
        {
            if (disparo == 2)
            {
                disparo = 1;
                vida1.SetActive(false);
                ps2.Play();
            }
            else if (disparo == 1)
            {
                disparo = 0;
                vida2.SetActive(false);
                ps3.Play();
            }
            else if (disparo == 0)
            {
                StartCoroutine(DestroyShip());
            }
        }
        else if (other.tag == "Wall")
        {
            Destroy(this.gameObject);
        }
    }


    IEnumerator DestroyShip()
    {
        Imdead = true;
        //Desactivo el grafico
        sprite[elegido].SetActive(false);

        sm.AddScore(250);
        vida3.SetActive(false);
        //Elimino el BoxCollider2D
        collider.enabled = false;

        //Lanzo la partícula
        ps.Play();

        //Lanzo sonido de explosion
        if (effects == 1)
        {
            audioSource.Play();
        }

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.5f);

        Destroy(this.gameObject);
    }

    IEnumerator DestroyShip2()
    {
        Imdead = true;
        //Desactivo el grafico
        sprite[elegido].SetActive(false);

        sm.AddScore(250);

        //Elimino el BoxCollider2D
        collider.enabled = false;

        //Lanzo sonido de explosion
        if (effects == 1)
        {
            audioSource.Play();
        }

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.5f);

        Destroy(this.gameObject);
    }
}
