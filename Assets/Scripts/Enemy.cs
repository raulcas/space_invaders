﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] GameObject[] sprite;
    [SerializeField] GameObject vida;
    private int elegido;
    private bool Imdead = false;

    [SerializeField] BoxCollider2D collider;
    [SerializeField] ParticleSystem ps;
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioSource audioSource2;

    private ScoreManager sm;
    private float timeCounter;
    private float timeToShoot;

    private float timeShooting;
    private float speedx;

    private bool isShooting;

    [SerializeField] GameObject bullet;
    private int effects = 1;

    private void Awake()
    {
        effects = PlayerPrefs.GetInt("effects", 0);

        elegido = Random.Range(0, sprite.Length);

        for (int kk = 0; kk < sprite.Length; kk++)
        {
            sprite[kk].SetActive(false);
        }

        sprite[elegido].SetActive(true);

        sm = (GameObject.Find("ScoreManager")).GetComponent<ScoreManager>();

        Inicitialization();
    }

    protected virtual void Inicitialization()
    {
        timeCounter = 0.0f;
        timeToShoot = 1.0f;
        timeShooting = 1.0f;
        speedx = 3.0f;
        isShooting = false;
    }

    protected virtual void EnemyBehaviour()
    {
        timeCounter += Time.deltaTime;

        if(Imdead == true)
        {
            return;
        }

        if (timeCounter > timeToShoot)
        {
            if (!isShooting)
            {
                isShooting = true;
                if (Imdead == false)
                {
                    Instantiate(bullet, this.transform.position, Quaternion.identity, null);
                    if (effects == 1)
                    {
                        audioSource2.Play();
                    }
                }
            }
            if (timeCounter > (timeToShoot + timeShooting))
            {
                timeCounter = 0.0f;
                isShooting = false;
            }
        }
        else
        {
            transform.Translate(-speedx * Time.deltaTime, 0, 0);
        }

    }

    void Update()
    {
        EnemyBehaviour();
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bullet")
        {
            StartCoroutine(DestroyShip());
        }
        else if (other.tag == "Bullet2")
        {
            StartCoroutine(DestroyShip2());
        }
        else if (other.tag == "Player")
        {
            StartCoroutine(DestroyShip());
        }
        else if (other.tag == "Wall")
        {
            Destroy(this.gameObject);
        }
    }


    IEnumerator DestroyShip()
    {
        Imdead = true;
        //Desactivo el grafico
        sprite[elegido].SetActive(false);
        vida.SetActive(false);
        sm.AddScore(100);

        //Elimino el BoxCollider2D
        collider.enabled = false;

        //Lanzo la partícula
        ps.Play();

        //Lanzo sonido de explosion
        if (effects == 1)
        {
            audioSource.Play();
        }

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.5f);

        Destroy(this.gameObject);
    }

    IEnumerator DestroyShip2()
    {
        Imdead = true;
        //Desactivo el grafico
        sprite[elegido].SetActive(false);
        vida.SetActive(false);
        sm.AddScore(100);

        //Elimino el BoxCollider2D
        collider.enabled = false;

        //Lanzo sonido de explosion
        if (effects == 1)
        {
            audioSource.Play();
        }

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.5f);

        Destroy(this.gameObject);
    }
}