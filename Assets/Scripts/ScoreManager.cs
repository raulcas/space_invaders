﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] Text score;

    private int scoreInt;

    void Start()
    {
        scoreInt = 0;
        score.text = scoreInt.ToString("000000");
    }

    public void AddScore(int value)
    {
        scoreInt += value;
        score.text = scoreInt.ToString("000000");
    }

    private void Update()
    {
        PlayerPrefs.SetFloat("score", scoreInt);
    }
}