﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class RocketBullet : MonoBehaviour
{
    public float speed;
    public GameObject explosion;
    public AudioSource audioSource;
    private int effects = 1;

    private void Awake()
    {
        effects = PlayerPrefs.GetInt("effects", 0);
    }
    
    void Update () {
        transform.Translate (speed * Time.deltaTime,0, 0);
    }
 
    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Wall") {
            Destroy(this.gameObject);
        }
        else if (other.tag == "Enemy") {
            if (effects == 1)
            {
                audioSource.Play();
            }

            Instantiate(explosion, this.transform.position, Quaternion.identity, null);
            GameObject go = Instantiate(explosion, this.transform.position, Quaternion.identity, null);
            go.transform.Rotate(0, 0, 30);
            GameObject go2 = Instantiate(explosion, this.transform.position, Quaternion.identity, null);
            go2.transform.Rotate(0, 0, -30);
            GameObject go3 = Instantiate(explosion, this.transform.position, Quaternion.identity, null);
            go3.transform.Rotate(0, 0, 60);
            GameObject go4 = Instantiate(explosion, this.transform.position, Quaternion.identity, null);
            go4.transform.Rotate(0, 0, -60);
            GameObject go5 = Instantiate(explosion, this.transform.position, Quaternion.identity, null);
            go5.transform.Rotate(0, 0, 90);
            GameObject go6 = Instantiate(explosion, this.transform.position, Quaternion.identity, null);
            go6.transform.Rotate(0, 0, -90);
            GameObject go7 = Instantiate(explosion, this.transform.position, Quaternion.identity, null);
            go7.transform.Rotate(0, 0, 120);
            GameObject go8 = Instantiate(explosion, this.transform.position, Quaternion.identity, null);
            go8.transform.Rotate(0, 0, -120);
            GameObject go9 = Instantiate(explosion, this.transform.position, Quaternion.identity, null);
            go9.transform.Rotate(0, 0, 150);
            GameObject go10 = Instantiate(explosion, this.transform.position, Quaternion.identity, null);
            go10.transform.Rotate(0, 0, -150);
            GameObject go11 = Instantiate(explosion, this.transform.position, Quaternion.identity, null);
            go11.transform.Rotate(0, 0, 180);
            Destroy(this.gameObject);
        }
    }
}