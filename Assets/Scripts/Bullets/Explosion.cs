﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public float explosion;
    private float currentTime = 0;
    public float speed;

    void Update()
    {
        transform.Translate(speed * Time.deltaTime, 0, 0);

        currentTime += Time.deltaTime;

        if(currentTime >= explosion)
        {
            Destroy(this.gameObject);
        }
    }
}
